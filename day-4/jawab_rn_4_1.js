/*
Soal No. 1 

Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai “Halo Sanbers!” 
yang kemudian dapat ditampilkan di console.

*/

function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak());