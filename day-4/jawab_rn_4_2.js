/*
Soal No. 2 
Tulislah sebuah function dengan nama kalikan() yang mengembalikan 
hasil perkalian dua parameter yang di kirim.

*/

function kalikan(num1, num2) {
    return num1*num2;
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
