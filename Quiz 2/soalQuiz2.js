/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam letiabel, misal let a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    constructor(subject,points,email) {
      this.subject=subject;
      this.points=points; 
      this.email=email
    }
    average = () => {
      let p=this.points.length;
      if (p==0){
        return 0;
      } else  {
        let jml=0;
        for (let i=0;i<p;i++){
          jml=jml+this.points[i];
        }
        return Math.round(jml/p * 10) / 10 ;
      }
    }
}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

const viewScores = (data, subject) => {
  let scores=[];
  let d=[];
  for (let i=0;i<data.length;i++){
    if (i==0) {
      for (let j=0;j<data[0].length;j++){
        d[j]= data[0][j];
      }
    } else {
      //let p=d.findIndex(subject);
      let p=0;
      for (let j=1;j<d.length;j++){
        if (subject==d[j]) {
          p=j;
        }
      }
      score =  {
        email: data[i][0],
        subject: subject, 
        point: data[i][p]
      }
      scores.push(score);
   }
  }
  console.log(scores);
}

// TEST CASE
viewScores(data, "quiz - 1")
viewScores(data, "quiz - 2")
viewScores(data, "quiz - 3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

const recapScores = (data) => {
  for (let i=1;i<data.length;i++){
    const score=new Score('nilai',[ data[i][1],data[i][2],data[i][3] ],data[i][0]);
    let rata2 = score.average();
    let predikat = rata2>90 ? "honour" : rata2>80 ? "graduate" : rata2>70 ? "participant" : "";
    console.log(i, 'Email: '+score.email,'\n  Rata-rata: '+rata2,'\n  Predikat: '+predikat);
  }
}

recapScores(data);
