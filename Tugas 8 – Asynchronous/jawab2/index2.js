var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 

// Lanjutkan code untuk menjalankan function readBooksPromise 


function start(times) {
  if (times>0) {
     readBooksPromise(times,books[i])
     .then(hasil => start(hasil))
     .catch(error => console.log(error))
  }
  i++
}

i=0
start(8000)

/* tampilan

saya mulai membaca LOTR
saya sudah selesai membaca LOTR, sisa waktu saya 5000
saya mulai membaca Fidas
saya sudah selesai membaca Fidas, sisa waktu saya 3000
saya mulai membaca Kalkulus
saya sudah tidak punya waktu untuk baca Kalkulus
-1000


*/


