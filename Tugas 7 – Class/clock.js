class Clock {
    constructor(temp) {
      this.template=temp.template;
      this.timer=null;   
    }

    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
      
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
      //if (output=='13:36:00') this.stop();
    }
  
    stop() {
      clearInterval(this.timer);
    };
  
    start() {
      const render=()=>this.render();
      this.timer = setInterval(render, 1000);
    };
  
}
  
var clock = new Clock({template: 'h:m:s'});
clock.start();  
