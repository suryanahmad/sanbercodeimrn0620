var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)


function arrayToObject(arr) {
    if (arr.length) {
        var i=1;
        arr.forEach(dt => {  
            myObject =  {
                firstName: dt[0], 
                lastName: dt[1], 
                gender: dt[2], 
                age: (dt[3]==null) || (dt[3] > thisYear) ? "Invalid birth year" : thisYear-dt[3]
            }
            console.log(i+". "+myObject.firstName+" "+myObject.lastName+":",myObject);
            i++;
        });
    }
    else {
        console.log("")
    }
    console.log("=========================\n");

}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
