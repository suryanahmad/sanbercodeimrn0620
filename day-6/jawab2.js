//maka output:

//{ memberId: ‘324193hDew2’, money: 700000, listPurchased: [ ‘Baju Zoro’, ‘Sweater Uniklooh’ ], changeMoney: 25000 }

//Code

function shoppingTime(memberId, money) {
    var listSale = [{brand:"Sepatu Stacattu",harga:1500000},{brand:"Baju Zoro",harga:500000},{brand:"Baju H&N",harga : 250000},
    {brand:"Sweater Uniklooh",harga: 175000},{brand:"Casing Handphone",harga:50000}];

    listSale.sort(function(a,b) {return b.harga-a.harga});
    if (memberId==null||memberId==''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money<listSale[listSale.length-1].harga){
        return "Mohon maaf, uang tidak cukup";
    } else {
        const memberObject = {
            memberId: memberId,
            money: money,
            listPurchased:[],
            changeMoney:money
        }
        for (var i=0; i<listSale.length; i++) {
            if (memberObject.changeMoney>=listSale[i].harga){
                memberObject.listPurchased.push(listSale[i].brand)
                memberObject.changeMoney -= listSale[i].harga;
            }    
        }
        return memberObject;
    }

    
  // you can only write your code here!
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

