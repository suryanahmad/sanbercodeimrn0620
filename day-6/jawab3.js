//Soal No. 3 (Naik Angkot)
//Problem

//Diberikan function naikAngkot(listPenumpang) yang akan menerima satu parameter berupa array dua dimensi. 
//Function akan me-return array of object.

//Diberikan sebuah rute, dari A – F. Penumpang diwajibkan membayar Rp2000 setiap melewati satu rute.

//Contoh: input: [ [‘Dimitri’, ‘B’, ‘F’] ] output: [{ penumpang: ‘Dimitri’, naikDari: ‘B’, tujuan: ‘F’, bayar: 8000 }]

//Code

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var arrObject=[];
    arrPenumpang.forEach(t => {  
        var bayar=0;
        var tambah=0;
        for (var i=0;i<rute.length;i++) {
            bayar +=tambah;
            if (rute[i]==t[1]){
                tambah=200;
            } else if (rute[i]==t[2]){
                tambah=0;
            }
        }
        var myObject =  {
            penumpang: t[0], 
            naikDari: t[1], 
            tujuan: t[2], 
            bayar: bayar
        }
        arrObject.push(myObject);
    });
    return arrObject;
}
      

 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]