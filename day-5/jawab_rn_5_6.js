// push => insert last
// pop => delete last
// shift => insert first 
// unshift => delete first
// join => array to string
// split => string to array
// slice => slice(1,3): subArray from index 1 to 3
// splice => splice(1, 0, "watermelon"): replace | delete 0 element et 1 and insert "watermelon" at 1
// sort

function dataHandling2(input){
    var hasil1=[...input];
    hasil1.splice(4,1,"Pria", "SMA Internasional Metro");
    hasil1[1]=hasil1[1]+"Elsharawy";
    hasil1[2]="Provinsi "+hasil1[2];
    console.log(hasil1);

    var hasil2=input[3];
    hasil2=hasil2.split("/")[1];
    var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", 
                 "Agustus", "September", "Oktober", "November", "Desember"];
    hasil2 = bulan[hasil2-1];
    console.log(hasil2);

    var hasil3=input[3];
    hasil3=hasil3.split("/");
    hasil3.reverse();
    console.log(hasil3);

    var hasil4=input[3];
    hasil4=hasil4.split("/");
    hasil4=hasil4.join("-");
    console.log(hasil4);

    var hasil5=input[1];
    console.log(hasil5);
    
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 