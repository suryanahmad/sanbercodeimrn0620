
function sum(startNum, finishNum, step) {
    if (startNum==null && finishNum==null)
        return 0;
    else if (finishNum==null)
        return 1;
    else if (startNum<=finishNum) {
        if (step==null) step=1;
        var hasil=0
        for (var i=startNum; i<=finishNum; i = i+step){
            hasil=hasil+i;
        }
        return hasil;
    } else {
        if (step==null) step=1;
        var hasil=0;
        for (var i=startNum; i>=finishNum; i = i-step){
            hasil=hasil+i;
        }
        return hasil;
    }
}

// Code di sini
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 