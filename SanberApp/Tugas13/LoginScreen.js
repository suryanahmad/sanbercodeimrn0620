import React, { Component } from 'react'
import { View, StyleSheet, Text, Div, Button, Image, TextInput } from 'react-native'

export default class LoginScreen extends Component {

  state = {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    const primaryAxis = flexDirection === 'row' ? 'Horizontal' : 'Vertical'
    const secondaryAxis = flexDirection === 'row' ? 'Vertical' : 'Horizontal'

    return (
      <View style={styles.container}>
        <View style={styles.container2}>
          <Image style={styles.gambar}
            source={require('./images/logo.png')}/>
          <Text style={styles.judul}>Login</Text>
        </View>
        <View style={styles.container1}>
          <Text style={styles.input}>Username / Email</Text>
          <TextInput style={styles.box} />
        </View>
        <View style={styles.container1}>
          <Text style={styles.input}>Password</Text>
          <TextInput style={styles.box} secureTextEntry={true}/>
        </View>
        <View style={styles.container2}>
          <Button style={styles.tombol} title="  Masuk  " />
          <Text style={styles.tengah}>atau</Text>
          <Button style={styles.tombol} title="Daftar ?" />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  container1: {
    marginLeft: 30,
  },
  container2: {
    marginTop: 15,
    alignItems: "center"
  },
  layout: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  gambar: {
    width:330, height: 100, marginTop: 50
  },
  box: {
    padding: 5,
    backgroundColor: 'steelblue',
    marginTop: 5,
    marginBottom: 5,
    width: 300,
    alignItems: 'stretch'
  },
  tombol: {
    marginTop: 15,
    marginBottom: 15,
    color: "#841584",
    alignItems: 'center',
    borderRadius: 10
  },
  judul: {
    marginTop: 10,
    marginBottom: 25,
    fontSize:32,
    alignItems: 'center',
  },
  tengah: {
    alignItems: 'center'
  },
  input: {
    marginLeft: 5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
})