import React, { Component } from 'react'
import { View, StyleSheet, Text, Div, Button, Image } from 'react-native'

export default class RegisterScreen extends Component {

  state = {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    const primaryAxis = flexDirection === 'row' ? 'Horizontal' : 'Vertical'
    const secondaryAxis = flexDirection === 'row' ? 'Vertical' : 'Horizontal'

    return (
      <View style={styles.container}>
      <Image style={{width:330, height: 100, marginTop: 50}}
            source={require('./images/logo.png')}/>
           <View style={styles.judul}>
              <Text style={styles.judul}>Register</Text>
           </View>
           <View>
              <Text style={styles.input}>Username</Text>
           </View>
           <View style={styles.box} />
           <View>
              <Text style={styles.input}>Email</Text>
           </View>
           <View style={styles.box} />
           <View style={styles.input}>
              <Text>Password</Text>
           </View>
           <View style={styles.box} />
           <View style={styles.input}>
              <Text>Ulangi Password</Text>
           </View>
           <View style={styles.box} />
           <View style={[{ width: "25%", alignItems: "center"}]}>
              <Button
                 title=" Daftar "
                 color="#841584"
                 align='center'
              />
           </View>
           <View style={styles.tengah}>
              <Text>atau</Text>
           </View>
           <View style={[{ width: "25%", alignItems: "center"}]}>
              <Button
                 title="Masuk ?"
                 color="#841584"
                 align='center'
              />
           </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: "center"
  },
  layout: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  box: {
    padding: 15,
    backgroundColor: 'steelblue',
    margin: 5,
    width: 200,
    alignItems: 'stretch'
  },
  tombol: {
    color: "#841584",
  },
  judul: {
    marginTop: 5,
    marginBottom: 10,
    fontSize:32,
    alignItems: 'center'
  },
  tengah: {
    alignItems: 'center'
  },
  input: {
    marginLeft: 5,
    alignItems: 'flex-start'
  }
})