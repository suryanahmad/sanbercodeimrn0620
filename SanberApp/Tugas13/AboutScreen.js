import React, { Component } from 'react'
import { View, StyleSheet, Text, Div, Button, Image, TextInput } from 'react-native'

export default class AboutScreen extends Component {

  state = {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    const primaryAxis = flexDirection === 'row' ? 'Horizontal' : 'Vertical'
    const secondaryAxis = flexDirection === 'row' ? 'Vertical' : 'Horizontal'

    return (
      <View style={styles.container}>
        <View style={styles.container2}>
          <Text style={styles.judul}>Tentang Saya</Text>
          <Image style={styles.gambar}
            source={require('./images/my_photo.png')}/>
          <Text style={styles.input}>Ahmad Suryan</Text>
          <Text style={styles.input}>React Native Developer</Text>
        </View>
        <Text style={styles.kiri}>Portofolio</Text>
        <View style={styles.garis}/>
        <Text style={styles.kiri}>Hubungi Saya</Text>
        <View style={styles.garis}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  container2: {
    alignItems: "center"
  },
  layout: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  garis: {
    padding: 1,
    backgroundColor: 'steelblue',
    width: 350,
    height: 1,
    alignItems: 'stretch'
  },
  gambar: {
    alignItems: "center",
    width:150, height: 150, marginTop: 5
  },
  box: {
    padding: 5,
    backgroundColor: 'steelblue',
    marginTop: 5,
    marginBottom: 5,
    width: 200,
    alignItems: 'stretch'
  },
  tombol: {
    marginTop: 15,
    marginBottom: 15,
    color: "#841584",
    alignItems: 'center',
    borderRadius: 10
  },
  judul: {
    marginTop: 30,
    marginBottom: 5,
    fontSize:32,
    alignItems: 'center',
  },
  tengah: {
    alignItems: 'center'
  },
  kiri: {
    marginTop: 30,
    alignItems: 'flex-start'
  },
  input: {
    marginLeft: 5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
})