import { StatusBar } from 'expo-status-bar';
import React, {Component } from 'react';
import { Platform, 
         StyleSheet, 
         Text, 
         View, 
         Image,
         TouchableOpacity,
         FlatList
       } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import data from './skillData.json';

export default class SkillScreen extends React.Component {
  render() {
    let skillData=data.items;
    return (
        <View style={styles.container}>
          <View style={styles.descContainer}>
             <FlatList
                ItemSeparatorComponent={ ({highlighted}) => (
                    <View style={[highlighted && {marginLeft: 0}]} />
                )}
                data={data.items}
                renderItem={({item, index, separators}) => (
                    <TouchableOpacity
                        key={item.key}>
                        <View style={{backgroundColor: 'lightblue'}}>
                            <Icon name={item.iconName} size={25} />
                            <Text>{item.skillName}</Text>
                            <Text>{item.categoryName}</Text>
                            <Text>{item.percentageProgress}</Text>
                            <Icon name='chevron-right' size={25} />
                        </View>
                    </TouchableOpacity>
                )}
              />
          </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:50
  },
  navBar: {
    marginTop: 21,
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c'
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },  
});
